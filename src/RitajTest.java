
import org.testng.Assert;
import org.testng.annotations.Test;


import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;




//the test will fails because the wrong password
public class RitajTest {

	@Test
	public void test(){

		System.setProperty("webdriver.gecko.driver", confgFile.firefoxDriverPath);
		WebDriver driver = new FirefoxDriver();

		driver.get("https://ritaj.birzeit.edu/register/?return_url=%2f");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.findElement(By.name("username")).sendKeys("1141908");
		driver.findElement(By.name("password")).sendKeys("moon@123");
		
		driver.findElement(By.name("formbutton:ok")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
		Assert.assertEquals("Welcome", driver.getTitle(),"wrong user name or password");

		driver.findElement(By.className("abb")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		System.out.println(driver.findElement(By.id("100")).getAttribute("style"));
		Assert.assertEquals("border: solid black 1px; border-top: 0;",
				driver.findElement(By.id("100")).getAttribute("style"));

		System.out.println("end testing");

	}
}
