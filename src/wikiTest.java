
import java.util.concurrent.TimeUnit;

//import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.Assert;
import org.testng.annotations.Test;


public class wikiTest {

	@Test
	public void test() {

		System.setProperty("webdriver.gecko.driver", confgFile.firefoxDriverPath);
		WebDriver driver = new FirefoxDriver();

		driver.get("https://www.wikipedia.org/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		driver.findElement(By.id("searchInput")).sendKeys("assert");

		//why assert not null here?  --> to make sure it found the element .. because i don't have a senario   
		Assert.assertNotNull(driver.findElement(By.xpath("//*[@id=\"search-form\"]/fieldset/button/i")));
		

		driver.findElement(By.xpath("//*[@id=\"search-form\"]/fieldset/button/i")).click();
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

		System.out.println("end testing");

	}
}
