
package jasReportPages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class LoginPage {
	
	private static WebElement element ; 
	
	public static WebElement txt_userName(WebDriver driver){
		element = driver.findElements(By.className("z-textbox")).get(0);
		return element;
	}
	
	public static WebElement txt_password(WebDriver driver){
		element = driver.findElements(By.className("z-textbox")).get(1);
		return element;
	}
	
	public static WebElement Button_login(WebDriver driver){
		element = driver.findElements(By.className("z-button-cm")).get(0);
		return element;
	}
	
	

}
