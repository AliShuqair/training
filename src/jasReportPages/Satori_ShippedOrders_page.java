package jasReportPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class Satori_ShippedOrders_page {

	private static WebElement element;
	private static List<WebElement> elements;
	
	public static List<WebElement> frames(WebDriver driver) {
		elements = driver.findElements(By.className("z-iframe"));
		return elements;
	}
	
	public static WebElement find_img(WebDriver driver) {
		element = driver.findElement(By.id("__bookmark_1"));
		return element;
	}
	
	
}
