
package jasReportPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class homePage {

	private static WebElement element;
	private static List<WebElement> elements;

	public static List<WebElement> taps_homePage(WebDriver driver) {
		elements = driver.findElements(By.className("z-tab-hr"));
		return elements;
	}

	public static List<WebElement> nag_homePage(WebDriver driver) {
		elements = driver.findElements(By.className("z-tabpanel-accordion-lite-outer"));
		return elements;
	}

	public static WebElement frame_homePage(WebDriver driver) {
		element = driver.findElements(By.className("z-iframe")).get(1);
		return element;
	}
	
	public static WebElement menu_item_homePage(WebDriver driver) {
		element =driver.findElement(By.xpath("/html/body/div[1]/header/div[3]/div/div/div/div[1]/div/ul/li[1]/a"));
		return element;
	}

}
