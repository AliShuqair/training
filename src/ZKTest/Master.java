package ZKTest;

import java.util.concurrent.TimeUnit;


import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Test;



public class Master {

	/* 
	 * if You make the webdriver static, this means that you are using the same object over and over without creating an object from Master class
	 * This might trigger errors that are harder to trace
	 * You can use it as a class global to use the same object in multiple functions; which is also not preferred
	 * Remove the static, remove the global variable, and use SingleTone class design if you want to use the same instance of the driver
	 * and at the same time maintain the object life cycle
	 *  
	 */
	
	 WebDriver driver;
	
	/*
	 * This block of code inside start1, should be under @BeforeClass , as u r preparing the setup of the test
	 * It is not a testCase by itself
	 * Check @Before (used for setup) and @After (used for cleanup) annotations 
	 */
	@Test
	public void start1() throws InterruptedException{ //use descriptive function names
		
		System.setProperty("webdriver.gecko.driver", "C:\\drivers\\geckodriver.exe");//pls use config file for the location of the driver
		//System.setProperty("webdriver.gecko.driver","D:\\PDFS\\selinume_drivers\\geckodriver.exe");
		driver = new FirefoxDriver();

		driver.get("http://jasreport.atori.de/JASreport/");
		driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		
	}
	
	//this will test the log in page by enter wrong and correct information
	@Test
	public void start2() throws InterruptedException{//rename the functions 
		TestLogInPage logIn =new TestLogInPage("admin","admin");
		logIn.start(driver);
	}

	/*
	 *this will go to the tap number 5 and test if the photos are the correct
	 *p.s this test will fails because i don't know the origin photos
	 *i saved an old url but the photos are unstable 
	 */
	@Test
	public void start3() throws InterruptedException{
		TestImg mm = new TestImg();//use descriptive variable names
		mm.start(driver);
		
	}

	//this method will test if the taps in the main page are displayed Enabled
		@Test
		public void start4() throws InterruptedException{

			TestMainPage main = new TestMainPage();
			main.start(driver);

		}

}
