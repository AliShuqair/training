package ZKTest;
import java.util.concurrent.TimeUnit;//clean unused import 

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
//import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
//import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
//import org.testng.AssertJUnit;
import org.testng.annotations.Test;

public class TestLogInPage {

	
	private String userName;
	private String password;

	
	public TestLogInPage( String userName, String password) {
		this.userName = userName;
		this.password = password;
	}

	/*first i will test if the password or user name are not correct
	then i will log in with correct information 
	finally test if it was a successful log in*/

	@Test
	public void start(WebDriver driver) throws InterruptedException{// send the driver object to the function ,, do not use static reference for webdriver
		
		String title=driver.getTitle();
		Assert.assertEquals("JASreport", title);
		
		
		WebElement userFeild = driver.findElements(By.className("z-textbox")).get(0);
		//System.out.println("11111111");
		Assert.assertNotNull(userFeild);
        userFeild.clear();
        userFeild.sendKeys(userName);
        
        
        WebElement passFeild = driver.findElements(By.className("z-textbox")).get(1);
        Assert.assertNotNull(passFeild);
        passFeild.clear();
        passFeild.sendKeys(password+" *** ");
        
        WebElement okButton=driver.findElements(By.className("z-button-cm")).get(0);
        Assert.assertNotNull(okButton);
        okButton.click();
		Thread.sleep(2000);

        
        WebElement ok = driver.findElements(By.className("z-button-cm")).get(2);
        Assert.assertNotNull(ok);
        ok.click();	
        Thread.sleep(2000);
		
        
		userFeild.clear();
        userFeild.sendKeys(userName);
        
        passFeild.clear();
        passFeild.sendKeys(password);
        
        
        okButton.click();
        Thread.sleep(5000);
	}
	
	
}
