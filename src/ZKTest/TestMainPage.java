package ZKTest;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Action;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestMainPage {

	@Test
	public void start(WebDriver driver) throws InterruptedException{
		
        Thread.sleep(5000);
		Assert.assertEquals(driver.getCurrentUrl(),"http://jasreport.atori.de/JASreport/MainWindow.zul");
		List <WebElement> list1 = driver.findElements(By.className("z-tab-hr"));
		Assert.assertEquals(8,list1.size());
	
		list1.get(0).click();
		
		method1(list1);
		
		list1.get(0).click();
		
		List <WebElement> nav = driver.findElements(By.className("z-tabpanel-accordion-lite-outer"));
		Assert.assertEquals(nav.size(), 5);
		
		for (int i = 0; i < nav.size(); i++) {
			
			Assert.assertTrue(nav.get(i).isEnabled());
			nav.get(i).click();
		}
		
		Thread.sleep(2000);
		
		list1.get(0).click();
		
		WebElement frame = driver.findElements(By.className("z-iframe")).get(1);
		driver.switchTo().frame(frame);
		
		WebElement ele = driver.findElement(By.xpath("/html/body/div[1]/header/div[3]/div/div/div/div[1]/div/ul/li[1]/a"));
		
		Actions bulider = new Actions(driver);
		Action serise = bulider.moveToElement(ele).click().build();
		serise.perform();
		/*
		System.out.println("111");
		Actions actions = new Actions(driver);
		//action.doubleClick(list1.get(0)).build().perform();
		
		Action click = actions.moveToElement(list1.get(0)).doubleClick().click().build();
		click.perform();
		System.out.println("111");
		
		*/
		
	}
	
	@Test
	public void method1(List<WebElement> list){
		for (int i = 0; i < list.size()-2; i++) {
			if(i==0){
				
				Assert.assertTrue(list.get(6).isDisplayed());
				Assert.assertTrue(list.get(6).isEnabled());
				list.get(6).click();
				
				Assert.assertTrue(list.get(7).isDisplayed());
				Assert.assertTrue(list.get(7).isEnabled());
				list.get(7).click();;
			}
			Assert.assertTrue(list.get(i).isEnabled());
			Assert.assertTrue(list.get(i).isDisplayed());
			list.get(i).click();
			
		}
	}
	
	
	
}


