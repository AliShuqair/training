package ZKTest;
import java.util.List;

import org.omg.Messaging.SyncScopeHelper;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;

public class TestImg {
	
	List <WebElement> frame;
	String imgurlBefore[] = new String[5];
	String imgurlAfter[] = new String[5];
	
	@Test
	public void start(WebDriver driver) throws InterruptedException{
		Thread.sleep(5000);
		List <WebElement> list1 = driver.findElements(By.className("z-tab-hr"));
		Assert.assertEquals(8,list1.size());
		
		list1.get(5).click();
		Thread.sleep(3000);
		
	
		frame = driver.findElements(By.className("z-iframe"));
		
		
		for (int i = 1; i < imgurlBefore.length; i++) {
			driver.switchTo().frame(frame.get(i));
			imgurlBefore[i]=driver.findElement(By.id("__bookmark_1")).getAttribute("src");
			driver.switchTo().parentFrame();

		}
		
		Thread.sleep(61000);
		frame = driver.findElements(By.className("z-iframe"));
		
		for (int i = 1; i < imgurlAfter.length; i++) {
			driver.switchTo().frame(frame.get(i));
			imgurlAfter[i]=driver.findElement(By.id("__bookmark_1")).getAttribute("src");
			driver.switchTo().parentFrame();
		}
		
		
		
		
		for (int i = 1; i < imgurlAfter.length; i++) {	
			Assert.assertNotSame(imgurlBefore[i], imgurlAfter[i]);
//			System.out.println(imgurlBefore[i]);
//			System.out.println(imgurlAfter[i]);
//			System.out.println("\t\t********");
		}
		
		 
		//System.out.println(frame.size());
		
		//WebElement body =  driver.findElement(By.className("safari safari537 breeze"));
		
		
		
		/*
		imgurlBefore[1]  = "http://jasreport.atori.de/birt_4.3.2/preview?__sessionId=20171008_081421_671&__imageid=custom242ca06115ef96b57144.png";
		imgurlBefore[2]  = "http://jasreport.atori.de/birt_4.3.2/preview?__sessionId=20171008_102242_492&__imageid=custom242ca06115ef96b5714457.png";
		imgurlBefore[3]  = "http://jasreport.atori.de/birt_4.3.2/preview?__sessionId=20171008_081421_766&__imageid=custom242ca06115ef96b57147.png";
		imgurlBefore[4]  = "http://jasreport.atori.de/birt_4.3.2/preview?__sessionId=20171008_081522_418&__imageid=custom242ca06115ef96b571414.png";
		
			 
		
		for (int i = 1; i < frame.size(); i++) {
					testimgurl(frame.get(i),imgurlBefore[i],"__bookmark_1");
				}
		
		*/

	}
	

	private void testimgurl(WebElement frame,String imgurl,String id,WebDriver driver) {
		driver.switchTo().frame(frame);
		WebElement img2  = driver.findElement(By.id(id));
		Assert.assertEquals(imgurl,img2.getAttribute("src"));
	}
	
	/*
	
	@Test
	public void Test1(){
		driver.switchTo().frame(this.frame.get(1));
		WebElement img  = driver.findElement(By.id("__bookmark_1"));
		Assert.assertEquals(imgurl[1],img.getAttribute("src"));
	}
	@Test
	public void Test2(){
		driver.switchTo().frame(this.frame.get(2));
		WebElement img  = driver.findElement(By.id("__bookmark_1"));
		Assert.assertEquals(imgurl[2],img.getAttribute("src"));
	}
	@Test
	public void Test3(){
		driver.switchTo().frame(this.frame.get(3));
		WebElement img  = driver.findElement(By.id("__bookmark_1"));
		Assert.assertEquals(imgurl[3],img.getAttribute("src"));
	}
	@Test
	public void Test4(){
		driver.switchTo().frame(this.frame.get(4));
		WebElement img  = driver.findElement(By.id("__bookmark_1"));
		Assert.assertEquals(imgurl[3],img.getAttribute("src"));
	}
	
	*/
	
}